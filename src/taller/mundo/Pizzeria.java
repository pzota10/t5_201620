package taller.mundo;

import taller.estructuras.Heap;


public class Pizzeria 
{ 
 // ----------------------------------
    // Constantes
    // ----------------------------------
 
 /**
  * Constante que define la accion de recibir un pedido
  */
 public final static String RECIBIR_PEDIDO = "RECIBIR";
 /**
  * Constante que define la accion de atender un pedido
  */
 public final static String ATENDER_PEDIDO = "ATENDER";
 /**
  * Constante que define la accion de despachar un pedido
  */
 public final static String DESPACHAR_PEDIDO = "DESPACHAR";
 /**
  * Constante que define la accion de finalizar la secuencia de acciones
  */
 public final static String FIN = "FIN";
 
 // ----------------------------------
    // Atributos
    // ----------------------------------
 
 /**
  * Heap que almacena los pedidos recibidos
  */
 // TODO 
 private Heap pedidosRecibidos;
 /**
  * Getter de pedidos recibidos
  */
 // TODO
 public Pedido getRecibido()
 {
  return (Pedido) pedidosRecibidos.peek();
 }
  /** 
  * Heap de elementos por despachar
  */
 // TODO
 private Heap paraDespachar;
 /**
  * Getter de elementos por despachar
  */
 // TODO 
 public Pedido getDespachar()
 {
  return (Pedido) paraDespachar.peek();
 }
 // ----------------------------------
    // Constructor
    // ----------------------------------

 /**
  * Constructor de la case Pizzeria
  */
 public Pizzeria()
 {
  // TODO 
  pedidosRecibidos= new Heap(5);
  paraDespachar= new Heap(5);
 }
 
 // ----------------------------------
    // M√©todos
    // ----------------------------------
 
 /**
  * Agrega un pedido a la cola de prioridad de pedidos recibidos
  * @param nombreAutor nombre del autor del pedido
  * @param precio precio del pedido 
  * @param cercania cercania del autor del pedido 
  */
 public void agregarPedido(String nombreAutor, double precio, int cercania)
 {
  // TODO
  Pedido p= new Pedido(nombreAutor, precio, cercania);
  pedidosRecibidos.add(p);
 }
 
 // Atender al pedido m√°s importante de la cola
 
 /**
  * Retorna el proximo pedido en la cola de prioridad o null si no existe.
  * @return p El pedido proximo en la cola de prioridad
  */
 public Pedido atenderPedido()
 {
  // TODO 
  Pedido resp=null;
  
  Pedido p = (Pedido) pedidosRecibidos.poll();

  if(p!=null)
  {
   p.setEstado(Pedido.DESPACHAR);
   paraDespachar.add(p);
   resp=p;
  }
  
  return resp;
 }

 /**
  * Despacha al pedido proximo a ser despachado. 
  * @return Pedido proximo pedido a despachar
  */
 public Pedido despacharPedido()
 {
  // TODO 
  Pedido p = (Pedido) paraDespachar.poll();
  return p;

 }
 
  /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
        int tamano = pedidosRecibidos.size();
        Pedido[] resp= new Pedido[tamano];
        
        for(int i=0; i<tamano;i++)
        {
         Pedido p = (Pedido) pedidosRecibidos.poll();
         resp[i]=p;
        }
        
        for(int i=0; i<tamano;i++)
        {
         Pedido p = (Pedido) resp[i];
         agregarPedido(p.getAutorPedido(), p.getPrecio(), p.getCercania());
        }
        
        return resp;
        
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
         int tamano = paraDespachar.size();
         Pedido[] resp= new Pedido[tamano];
         
         for(int i=0; i<tamano;i++)
         {
          Pedido p = (Pedido) paraDespachar.poll();
          resp[i]=p;
         }
         
         for(int i=0; i<tamano;i++)
         {
          Pedido p = (Pedido) resp[i];
          paraDespachar.add(p);
         }
         
         return resp;
     }
}
