package taller.mundo;

public class Pedido implements Comparable
{

 // ----------------------------------
 // Atributos
 // ----------------------------------
 public static final String RECIBIDO="recibido";
 
 public static final String DESPACHAR="despachado";
 
 private String estado;
 /**
  * Precio del pedido
  */
 private double precio;
 
 /**
  * Autor del pedido
  */
 private String autorPedido;
 
 /**
  * Cercania del pedido
  */
 private int cercania;
 
 // ----------------------------------
 // Constructor
 // ----------------------------------
 
 /**
  * Constructor del pedido
  * TODO Defina el constructor de la clase
  * @param cercania2 
  * @param precio2 
  * @param nombreAutor 
  */
 public Pedido(String nombreAutor, double precio2, int cercania2)
 {
  precio= precio2;
  autorPedido= nombreAutor;
  cercania= cercania2;
  estado=RECIBIDO;
 }
 
 // ----------------------------------
 // M√©todos
 // ----------------------------------
 
 /**
  * Getter del precio del pedido
  */
 public double getPrecio()
 {
  return precio;
 }
 
 /**
  * Getter del autor del pedido
  */
 public String getAutorPedido()
 {
  return autorPedido;
 }
 
 /**
  * Getter de la cercania del pedido
  */
 public int getCercania() {
  return cercania;
 }

 public String getEstado() {
  return estado;
 }

 public void setEstado(String estado) {
  this.estado = estado;
 }

 @Override
 public int compareTo(Object arg0) {
  // TODO Auto-generated method stub
  Pedido ped= (Pedido) arg0;
  int resp=0;
  if(estado.equals(RECIBIDO))
  {
   if(precio<ped.getPrecio())
   {
    resp=-1;
   }
   else
   {
    resp=1;
   }
  }
  else
  {
   if(cercania<ped.getCercania())
   {
    resp=1;
   }
   else
   {
    resp=-1;
   }
  }
  return resp;
 }
 
 // TODO 
}
