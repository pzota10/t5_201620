package taller.estructuras;

public class Heap<T extends Comparable<T>> implements IHeap {

 private T[] pq;
 
 private int N=0;
 
 public Heap(int maxN)
 {
  pq= (T[]) new Comparable[maxN+1];
 }
 
 
 @Override
 public void add(Object elemento) {
  // TODO Auto-generated method stub
  pq[++N] = (T) elemento;
   siftUp();
 }

 @Override
 public Object peek() {
  // TODO Auto-generated method stub
  return pq[0];
 }

 @Override
 public Object poll() {
  // TODO Auto-generated method stub
   T max = pq[1]; 
   exch(1, N--); 
   pq[N+1] = null; 
   siftDown(); 
   return max;
 }

 private void exch(int i, int j) {
  // TODO Auto-generated method stub
  T temp=pq[i];
  pq[i]=pq[j];
  pq[j]=temp;
 }


 @Override
 public int size() {
  // TODO Auto-generated method stub
  return N;
 }

 @Override
 public boolean isEmpty() {
  // TODO Auto-generated method stub
  return N==0;
 }

 @Override
 public void siftUp() {
  // TODO Auto-generated method stub
  int k=N;
  while (k > 1 && less(k/2, k))
   {
   exch(k/2, k);
   k = k/2;
   }
 }
 
 private boolean less(int i, int j)
 { return pq[i].compareTo(pq[j]) < 0; }

 @Override
 public void siftDown() {
  // TODO Auto-generated method stub
  int k=1;
  while (2*k <= N)
   {
   int j = 2*k;
   if (j < N && less(j, j+1)) j++;
   if (!less(k, j)) break;
   exch(k, j);
   k = j;
   } 
   
 }

}
